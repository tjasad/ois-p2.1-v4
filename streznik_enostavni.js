// Zaradi združljivosti razvoja na lokalnem računalniku vs. Cloud9 okolju
var port = process.env.PORT || 8080;

var http = require("http");
var steviloUporabnikov = 0;
http.createServer(function(zahteva, odgovor) {
  steviloUporabnikov++;
  odgovor.writeHead(200, {"Content-Type": "text/plain"});
  odgovor.end("Pozdravljen " + steviloUporabnikov + ". ljubitelj predmeta OIS!\n");
}).listen(port);

console.log("Strežnik je pognan!");